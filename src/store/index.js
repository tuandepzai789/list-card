import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user:{},
    username:""
  },
  getters: {
    username: state =>{
      return state.username;
    }
  },
  mutations: {
    SET_USER(state,value){
      state.user=value;
    },
    UPDATE_USERNAME(state,value){
      state.username=value;
    }
  },
  actions: {
  },
  modules: {
  }
})
