import Vue from 'vue'
import VueRouter from 'vue-router'
import StoreView from '../views/StoreView.vue'
import LoginPage from '@/views/LoginPage.vue'
import RegisterPage from '@/views/RegisterPage.vue'
import AdminPage from '@/views/AdminPage.vue'
// import axios from 'axios'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'StoreView',
    component: StoreView,
  },
  { path: '/login', name: 'LoginPage', component: LoginPage },
  { path: '/register', name: 'RegisterPage', component: RegisterPage },
  { path: '/admin', name: 'AdminPage', component: AdminPage }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
// const token = localStorage.getItem('token');
// router.beforeEach(async (to, from, next ) => {
//   try {
//     const res = await axios.get('/me');
//     console.log(to,from,next);
//     const status = res.status;
//     const me = res.data;
//     if(to.name !=='LoginPage' && (!token || !me || status ===401)){
//       next({name:'LoginPage'});
//     }else{
//       next();
//       if(to.name !=='LoginPage') router.app.$store.commit('SET_USER',me);
//     }
//   } catch (error) {
//     console.log(error);
//   }
// });
export default router
